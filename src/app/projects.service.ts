import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Project } from './project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private baseURL = "http://localhost:8088/api/projects";
  projet : Project[];
  constructor(private httpclient : HttpClient) { }

     getProjetList(): Observable<Project[]>{
     return this.httpclient.get<Project[]>(`${this.baseURL}`);
  }

  createProjet(projet: Project):Observable<Object>{
    return this.httpclient.post(`${this.baseURL}`, projet);
  }

  // getEmployeeById(id: number): Observable<Employee>{
  //   return this.httpClient.get<Employee>(`${this.baseURL}/${id}`);
  // }

  // updateEmployee(id: number, employee: Employee): Observable<Object>{
  //   return this.httpClient.put(`${this.baseURL}/${id}`, employee);
  // }

  // deleteEmployee(id: number): Observable<Object>{
  //   return this.httpClient.delete(`${this.baseURL}/${id}`);
  // }
}
