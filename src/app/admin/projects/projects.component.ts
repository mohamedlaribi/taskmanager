import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { ProjectsService } from 'src/app/projects.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  projetList : Project[];
  projet : Project = new Project();
  constructor(private projectService : ProjectsService,private router: Router) { }

  ngOnInit(): void {
    this.geToProjectList();
  }
  private geToProjectList(){
    this.projectService.getProjetList().subscribe(data => {
      this.projetList = data;
    });
  }


  saveEmployee(){
    this.projectService.createProjet(this.projet).subscribe( data =>{
      console.log(data);
      this.goToProjectList();
    },
    error => console.log(error));
  }
  goToProjectList(){
    this.router.navigate(['/dashboard']);
  }
  onSubmit(){
    console.log(this.projet);
    this.saveEmployee();
  }

}
